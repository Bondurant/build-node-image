FROM node:lts-alpine

ARG RUN_STATEMENTS

WORKDIR /app

COPY ./package.json /app

RUN ${RUN_STATEMENTS}
