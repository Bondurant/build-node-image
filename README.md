# BUILD NODE OR PYTHON IMAGE AND PUSH TO CONTAINER REGISTRY

# Overview
This demo shows how a custom node image can be created and pushed GitLab's container registry. The custom image can be any type of image, not just Node, as this method utilizes a YAML file that can accept arguments, making the process unique and customizable to any project. 

## Usage

1. Set up all your variables in your .gitlab.yml file. For now, the Dockerfile is still partially hard coded. 
2. Point to the build-docker-image.yml file to pass your variables from the .gitlab.yml file. 

# Graphical Overview
![Graphical overview of custom image creation and push to local container registry](build-image.png "File Overview")

## License
None
